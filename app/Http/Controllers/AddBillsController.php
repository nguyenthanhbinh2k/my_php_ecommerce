<?php

namespace App\Http\Controllers;

use App\Models\BillDetail;
use App\Models\Bills;
use App\Models\Cart;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AddBillsController extends Controller
{
    //

    public function addBill(Request $req){
        if(!Auth::check()){
            return redirect('/login');
        }
        else{
            $this->validate($req, 
                [
                    'phonnumber'=>'required|min:8|max:10',
                    'address'=>'required',
                ]
            );
            $id = Auth::user()->id;
            $mytime = Carbon::now('Asia/Ho_Chi_Minh');
            // echo $mytime; 
            // echo $req->phonnumber;
            // echo $req->address;
            // echo $id;
            $productUserBy = DB::table('carts')
            ->join('Products', 'idProduct','=','Products.id')
            ->where('idUser', '=', $id)
            ->select(
                'idSize',
                'idColor',
                'carts.quantity', 
                'carts.id as id',
                'carts.idProduct as idProduct',
                'carts.idUser as idUser',
                'Products.unitPrice as unitPrice', 
                )
            ->get();
            if(count($productUserBy)){
                $totalMoney = 0;
                foreach($productUserBy as $item){
                    $totalMoney += $item->quantity * $item->unitPrice;
                }
                $createBill = new Bills;
                $createBill->idCustomer = $id;
                $createBill->dateOrder = $mytime;
                $createBill->total = $totalMoney;
                $createBill->payment = 'COD';
                $createBill->status = 'pending';
                $createBill->address = $req->address;
                $createBill->phonenumber = $req->phonnumber;
                $createBill->save();
                
                foreach($productUserBy as $item){
                    $bill_detail = new BillDetail;
                    $bill_detail->idBill = $createBill->id;
                    $bill_detail->idProduct = $item->idProduct;
                    $bill_detail->idColor = $item->idColor;
                    $bill_detail->idSize = $item->idSize;
                    $bill_detail->quantity = $item->quantity;
                    $bill_detail->unitPrice = $item->unitPrice;
                    $bill_detail->save();
                }
                $deleteCart = DB::table('carts')->where('idUser','=',$id);
                $deleteCart->delete();
                return redirect('send-email');
            }
            else{
                return redirect()->back()->with('message', 'Your cart is empty');
            }
        }
        
        // return redirect()->back();
    }
}
