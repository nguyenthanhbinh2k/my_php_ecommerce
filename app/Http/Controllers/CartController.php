<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
class CartController extends Controller
{
    public function getCartuser(){
        if(!Auth::check()){
            return redirect('/login');
        }
        else {
            $id = Auth::user()->id;
            $cart = DB::table('carts')
            ->join('Products', 'idProduct','=','Products.id')
            ->join('Sizes','idSize','=','Sizes.id')
            ->join('Colors','idColor','=','Colors.id')
            ->where('idUser','=',$id)
            ->select('carts.quantity', 
                'carts.id as id',
                'carts.idProduct as idProduct',
                'carts.idUser as idUser',
                'Products.name as productName', 
                'Products.unitPrice as unitPrice', 
                'Products.image as image', 
                'Products.id as productid', 
                'Sizes.name as sizeName', 
                'Sizes.id as sizeid', 
                'Colors.name as colorName',
                'Colors.id as colorid')
            ->get();
            // echo $cart;
            $user = Auth::user();
            return view('page.shopping-cart', compact('cart', 'user'));
        }
    }

    public function deleteProduct($id){
        if(!Auth::check()){
            return redirect('/login');
        }
        else{
            $cartDelete = Cart::find($id);
            $cartDelete->delete();
            $id = Auth::user()->id;
            $cart = DB::table('carts')
            ->join('Products', 'idProduct','=','Products.id')
            ->join('Sizes','idSize','=','Sizes.id')
            ->join('Colors','idColor','=','Colors.id')
            ->where('idUser','=',$id)
            ->select('carts.quantity', 
                'carts.id as id',
                'Products.name as productName', 
                'carts.idProduct as idProduct',
                'carts.idUser as idUser',
                'Products.unitPrice as unitPrice', 
                'Products.image as image', 
                'Products.id as productid', 
                'Sizes.name as sizeName', 
                'Sizes.id as sizeid', 
                'Colors.name as colorName',
                'Colors.id as colorid')
            ->get();
            $user = Auth::user();
            return view('page.shopping-cart', compact('cart','user'));
        }
    }

    public function updateCart(Request $req){
        // print_r($req->numproduct);
        if(!$req['numproduct']){
            return redirect()->back()->with('message', 'Your cart is empty');
        }
        if(count($req['numproduct']) > 0){
            foreach($req['numproduct'] as $key=>$value){
                $infoCart = DB::table('carts')->where('id', '=', $key)->get();
                $inventory = DB::table('product_quantities')
                    ->where([
                        ['idProduct', '=', $infoCart[0]->idProduct],
                        ['idSize', '=', $infoCart[0]->idSize],
                        ['idColor', '=', $infoCart[0]->idColor],
                    ])->get();
                // echo $inventory;
                if($inventory[0]->quantity < $value){                    
                    // echo $cart;
                    $infoProduct = DB::table('products')->where('id','=',$inventory[0]->idProduct)->get();
                    return redirect()->back()->with('message', 'The number of products '. $infoProduct[0]->name .' is not enough'); 
                }
                else{
                    DB::table('carts')
                    ->where('id', '=', $key)
                    ->update(
                        ['quantity'=>$value]
                    );
                }
                
            }
        }
        // if(count($req['numproduct']) > 0){
        //     foreach($req['numproduct'] as $key=>$value){
        //         DB::table('carts')
        //         ->where('id', '=', $key)
        //         ->update(
        //             ['quantity'=>$value]
        //         );
        //     }
        // }
        $id = Auth::user()->id;
        $cart = DB::table('carts')
        ->join('Products', 'idProduct','=','Products.id')
        ->join('Sizes','idSize','=','Sizes.id')
        ->join('Colors','idColor','=','Colors.id')
        ->where('idUser','=',$id)
        ->select('carts.quantity', 
            'carts.id as id',
            'Products.name as productName', 
            'carts.idProduct as idProduct',
            'carts.idUser as idUser',
            'Products.unitPrice as unitPrice', 
            'Products.image as image', 
            'Products.id as productid', 
            'Sizes.name as sizeName', 
            'Sizes.id as sizeid', 
            'Colors.name as colorName',
            'Colors.id as colorid')
        ->get();
        $user = Auth::user();
        return view('page.shopping-cart', compact('cart','user')); 
        
    }
}
