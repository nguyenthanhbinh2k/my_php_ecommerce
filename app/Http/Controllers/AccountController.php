<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    //
    public function getAccount() {
        if(Auth::check()){
            $user = Auth::user();
            // echo $user;
            return view('page.account', compact('user'));
        }
        else{
            return view('page.account');
        }
        
    }

    public function updateProfile(Request $req){
        if(!Auth::check()){
            return redirect('/');
        }
        else {
            $users = Auth::user();
             DB::table('users')
            ->where('id', '=', $users->id)
            ->update([
                'name'=>$req->name, 
                'phonenumber'=>$req->phonenumber,
                'address'=>$req->address,
            ]);
            return redirect('/');
        }
    }
}
