<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin() {
        return view('page.login');
    }

    public function postLogin(Request $req) {
        $this->validate($req, 
            [
                'email'=>'required|email',
                'password'=>'required|min:6|max:20'
            ],
            [
                'email.required'=>"Please enter email",
                'email.email'=>"Email k dung dinh dang",
                'password.required'=>"Please enter password",
                'password.min'=>"Please must be than 6 character",
                'password.max'=>"Please must be less 20 character",
            ]
        );
        // $findEmail = array('email'=>$req->email);
        // if(!Auth::attempt($findEmail)){
        //     return redirect()->back()->with('message', 'Email not exsits');
        // }

        $credentials = array('email'=>$req->email, 'password'=>$req->password);
        if(Auth::attempt($credentials)){
            return redirect('home');
        }
        else{
            return redirect()->back()->with('message', 'Login fail');
        }
    }

    public function logoutuser() {
        Auth::logout();
        return redirect('home');
      }
}
