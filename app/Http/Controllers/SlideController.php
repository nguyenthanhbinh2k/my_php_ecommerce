<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class SlideController extends Controller
{
    public function AuthLogin()
    {
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('admin/dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }

    public function index()
    {
        $this->AuthLogin();
        $all_slides = DB::table('slides')->get();
        $manage_slides = view('page.admin.slides.all_slides')->with('all_slides',$all_slides);
        return view('page.admin_layout')->with('page.admin.slides.all_slides',$manage_slides);
    }

    public function edit_slide($id){
        $edit_slide = DB::table('slides')->where('id',$id)->first(); 
        $manage_slide = view('page.admin.slides.edit_slide')->with('edit_slide',$edit_slide);
        return view('page.admin_layout')->with('page.admin.slides.edit_slide',$manage_slide);
    }

    public function update_slide(Request $requests, $id){
        $data = array();

        $data['title'] = $requests->title;
        // $data['image'] = $requests->image;
        $data['thumb'] = $requests->thumb;

        DB::table('slides')->where('id',$id)->update($data);
        Session::put('message','Update slide success');
        return Redirect::to('admin/slides');
    }

    public function delete_slide($id){
        DB::table('slides')->where('id',$id)->delete();
        Session::put('message','Delete slide success');
        return Redirect::to('admin/slides');
    }
}
