<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class SizeController extends Controller
{
    public function AuthLogin()
    {
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('admin/dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }

    public function index()
    {
        $this->AuthLogin();
        $all_sizes = DB::table('sizes')->get();
        $manage_sizes = view('page.admin.sizes.all_sizes')->with('all_sizes',$all_sizes);
        return view('page.admin_layout')->with('page.admin.sizes.all_sizes',$manage_sizes);
    }

    public function edit_size($id){
        $edit_size = DB::table('sizes')->where('id',$id)->first(); 
        $manage_size = view('page.admin.sizes.edit_size')->with('edit_size',$edit_size);
        return view('page.admin_layout')->with('page.admin.sizes.edit_size',$manage_size);
    }

    public function update_size(Request $requests, $id){
        $data = array();

        $data['name'] = $requests->name;

        DB::table('sizes')->where('id',$id)->update($data);
        Session::put('message','Update size success');
        return Redirect::to('/admin/sizes');
    }

    public function delete_size($id){
        DB::table('sizes')->where('id',$id)->delete();
        Session::put('message','Delete size success');
        return Redirect::to('page.admin.sizes');
    }
}
