<?php

namespace App\Http\Controllers;
use App\Models\Categorys;
use DB;
use Session;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Categorys :: all();
        $allCategories = view('page.admin.category.list-category')->with('categories', $categories);
        return view('page.admin_layout')->with('page.admin.category.list-category', $allCategories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.admin.category.add-category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newCategory = new Categorys;
        $newCategory->name = $request->name;
        if($newCategory->save()){
            return Redirect::to('admin/list-category');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Categorys::find($id);
        return $category->name;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $category = Categorys::find($id);

        $page_edit= view('page.admin.category.edit-category')->with('category',  $category);
        
        return view('page.admin_layout')->with('page.admin.category.edit-category', $page_edit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = array();
        $category['name'] = $request->name;
        DB::table('categorys')->where('id', $id)->update($category);
        return Redirect::to('admin/list-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('categorys')->where('id', $id)->delete();
        return Redirect::to('admin/list-category');
    }
}
