<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class UserController extends Controller
{
    public function AuthLogin()
    {
        $id = Session::get('admin_id');
        if($id){
            return Redirect::to('admin/dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }

    public function index()
    {
        $this->AuthLogin();
        $all_user = DB::table('users')->get();
        $manage_user = view('page.admin.users.all_users')->with('all_users',$all_user);
        return view('page.admin_layout')->with('page.admin.users.all_users',$manage_user);
    }

    public function add_users(){
        $this->AuthLogin();
        return view('page.admin.users.add_users');
    }

    public function store_users(Request $requests){
        $data = array();
        $data['name'] = $requests->name;
        $data['email'] = $requests->email;
        $data['phonenumber'] = $requests->phonenumber;
        $data['address'] = $requests->address;
        $data['password'] = md5($requests->password);

        DB::table('users')->insert($data);
        Session::put('message','Add user success');
        return Redirect::to('add-users');
    }

    public function edit_user($id){
        $edit_user = DB::table('users')->where('id',$id)->first(); 
        $manage_user = view('page.admin.users.edit_user')->with('edit_user',$edit_user);
        return view('page.admin_layout')->with('page.admin.users.edit_user',$manage_user);
    }

    public function update_user(Request $requests, $id){
        $data = array();

        $data['name'] = $requests->name;
        $data['phonenumber'] = $requests->phonenumber;
        $data['address'] = $requests->address;

        DB::table('users')->where('id',$id)->update($data);
        Session::put('message','Update user success');
        return Redirect::to('users');
    }

    public function delete_user($id){
        DB::table('users')->where('id',$id)->delete();
        Session::put('message','Delete user success');
        return Redirect::to('users');
    }
}
