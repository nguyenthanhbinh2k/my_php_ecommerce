<?php
 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendEmail()
    {
        $user = Auth::user();
        $to_name = "Coza Store";
        $to_email = $user->email;
        $data = array("name"=>$user->name);
        Mail::send('template.mail', $data, function($message) use ($to_name, $to_email){
            $message->to($to_email)->subject('Thank you to the customer');
            $message->from($to_email, $to_name);
        });
        return redirect('home');
    }
}