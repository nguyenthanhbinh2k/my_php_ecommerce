<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function getRegiser() {
        return view('page.register');
    }

    public function postRegister(Request $req){
        $this->validate($req,
            [
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:6|max:20',
                'name'=>'required',
                're_password'=>'required|same:password'
            ],
            [
                'email.required'=>'Please enter email',
                'email.email'=>'Email fomat not correct',
                'email.unique'=>'Email exsits',
                'password.required'=>'Please enter password',
                're_password.same'=>'Comfirm password not correct',
                'password.min'=>'Password must be than 6 character'
        ]);
        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->address = $req->address;
        $user->phonenumber = $req->phonenumber;
        $user->password = Hash::make($req->password);
        $user->save();
        // return redirect()->back()->with('success', 'Create account success');
        return redirect('/login');
    }
}
