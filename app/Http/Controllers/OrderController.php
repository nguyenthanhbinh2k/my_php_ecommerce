<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class OrderController extends Controller
{
    public function AuthLogin()
    {
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('admin/dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }

    public function index()
    {
        $this->AuthLogin();
        $all_orders = DB::table('bills')
        ->join('users','bills.idCustomer','=','users.id')
        ->select('bills.*','users.name')
        ->get();
        $manage_orders = view('page.admin.orders.all_orders')->with('all_orders',$all_orders);
        return view('page.admin_layout')->with('page.admin.orders.all_orders',$manage_orders);
    }

    public function view_order($id)
    {
        $this->AuthLogin();
        $order = DB::table('bills')
        ->join('users','bills.idCustomer','=','users.id')
        ->join('bill_details','bills.id','=','bill_details.idBill')
        ->join('products','bill_details.idProduct','=','products.id')
        ->join('sizes','bill_details.idSize','=','sizes.id')
        ->join('colors','bill_details.idColor','=','colors.id')
        ->select('bills.address as placeOfReceipt','bills.phonenumber as phoneOfReceipt','bill_details.quantity as productQuantity','bill_details.unitPrice as productUnitPrice','users.name as userName','users.*','products.*','colors.name as colorName','sizes.name as sizeName')
        ->where('bills.id',$id)
        ->get();
        $order_by_id = view('page.admin.orders.view_order')->with('order_by_id',$order);
        return view('page.admin_layout')->with('page.admin.orders.view_order',$order_by_id);
    }

    public function approve_status($id){
        $flag = 0;
        $listItem = DB::table('bill_details')->where('bill_details.idBill', $id)->get();

        foreach($listItem as $item){
            $data = DB::table('product_quantities')
            ->where([
                ['idProduct', '=', $item->idProduct],
                ['idSize', '=', $item->idSize],
                ['idColor', '=', $item->idColor],
            ])->get();

            if($data[0]->quantity < $item->quantity) {
                $flag = 1;
            }           
        }

        if($flag == 0) {
            foreach($listItem as $item){
                $data = DB::table('product_quantities')
                ->where([
                    ['idProduct', '=', $item->idProduct],
                    ['idSize', '=', $item->idSize],
                    ['idColor', '=', $item->idColor],
                ])->get();

                $updateQuantity = array();
                $updateQuantity['quantity'] = $data[0]->quantity - $item->quantity;
                DB::table('product_quantities')->where([
                    ['idProduct', '=', $item->idProduct],
                    ['idSize', '=', $item->idSize],
                    ['idColor', '=', $item->idColor],
                ])->update($updateQuantity);
            }

            $updateInfo = array();

            $updateInfo['status'] = 'Approved';
            DB::table('bills')->where('id',$id)->update($updateInfo);
            Session::put('message','Update order status success');
        } else {
            Session::put('message','Do not enough products to approve order');
        }
        
        return Redirect::to('manage-order');
    }

    public function decline_status($id){
        $data = array();

        $data['status'] = 'Declined';
        DB::table('bills')->where('id',$id)->update($data);
        Session::put('message','Update order status success');
        return Redirect::to('manage-order');
    }
}
