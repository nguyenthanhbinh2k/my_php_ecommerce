<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class ColorController extends Controller
{
    public function AuthLogin()
    {
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('admin/dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }

    public function index()
    {
        $this->AuthLogin();
        $all_colors = DB::table('colors')->get();
        $manage_colors = view('page.admin.colors.all_colors')->with('all_colors',$all_colors);
        return view('page.admin_layout')->with('page.admin.colors.all_colors',$manage_colors);
    }

    public function edit_color($id){
        $edit_color = DB::table('colors')->where('id',$id)->first(); 
        $manage_color = view('page.admin.colors.edit_color')->with('edit_color',$edit_color);
        return view('page.admin_layout')->with('page.admin.colors.edit_color',$manage_color);
    }

    public function update_color(Request $requests, $id){
        $data = array();

        $data['name'] = $requests->name;

        DB::table('colors')->where('id',$id)->update($data);
        Session::put('message','Update color success');
        return Redirect::to('admin/colors');
    }

    public function delete_color($id){
        DB::table('colors')->where('id',$id)->delete();
        Session::put('message','Delete color success');
        return Redirect::to('admin/colors');
    }
}
