<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Http\Middleware\Product;
use App\Models\ImageDetail;
use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Slide;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Categorys;
use App\Models\Sizes;
use App\Models\Colors;
use App\Models\ProductQuantity;
use Illuminate\Support\Facades\Redirect;
class ProductController extends Controller
{
    // Tao 1 san pham
    public function createProduct(){
        $products = new Products;
        $products->idCategory = 1;
        $products->idSize = 1;
        $products->name = 'Ao khoac';
        $products->desc = 'Ao khoac long vu sieu ben';
        $products->gender = false;
        $products->quantity = 20;
        $products->unitPrice = 500000;
        $products->image = 'https://localbrand.vn/wp-content/uploads/2020/12/ao-hoodie-cua-local-brand-2.jpg';
        $products->save();
    }

    public function getHome(){
        // $products = Products::find(1);
        $slide = Slide::all();
        // $products = Products::paginate(8);
        $products = DB::table('products')->orderBy('idCategory','asc')->paginate(8);
        return view('page.cpn_home_list_product', compact('slide', 'products'));
    }

    public function getProduct($type){
        if($type==0){
            // $productByCate = Products::paginate(8);
            $productByCate = DB::table('products')->orderBy('idCategory','asc')->paginate(8);
        return view('page.product', compact('productByCate'));
        }
        $productByCate = DB::table('products')->where('idCategory', $type)->orderBy('created_at','desc')->paginate(8);
        // $productByCate = Products::paginate(8)->where('idCategory', $type)->get();
        // $productByCate = Products::where('idCategory', $type)->get();
        return view('page.product', compact('productByCate'));
    }

    public function getProductDetail(Request $req){
        $product = Products::where('id', $req->id)->first();
        // $color_detail = ProductQuantity::where('idProduct', $req->id)->get();
        $colors = DB::table('product_quantities')
            ->join('Colors', 'idColor', '=', 'Colors.id')
            ->where('idProduct', $req->id)
            ->select(['Colors.name', 'Colors.id'])
            ->groupBy(['Colors.name', 'Colors.id'])
            ->get();
        $sizes = DB::table('product_quantities')
            ->join('Sizes', 'idSize', '=', 'Sizes.id')
            ->where('idProduct', $req->id)
            ->select(['Sizes.name', 'Sizes.id'])
            ->groupBy(['Sizes.name', 'Sizes.id'])
            ->get();
        $quantity = DB::table('product_quantities')
            ->where('idProduct', '=' , $req->id)
            ->sum('product_quantities.quantity');
        $image_detail = ImageDetail::where('id', $product->idImage)->first();
        $category = DB::table('categorys')->where('id', $product->idCategory)->get();
        // echo $category;
        return view('page.product-detail', compact('product', 'colors', 'sizes', 'quantity', 'image_detail', 'category'));
    }


    public function addToCart(Request $req){
        $message = [
                'gt'=>'you must choose :attribute ',
                
        ];
        $this->validate($req, 
            [
                'size'=>'required|numeric|gt:0',
                'color'=>'required|numeric|gt:0',
                'numproduct'=>'required|numeric|gt:0',
            ],$message
        );
        $quantity = DB::table('product_quantities')->where([
            ['idProduct', '=',$req->idProduct],
            ['idSize','=', $req->size],
            ['idColor','=', $req->color],
        ])->select('quantity') ->get();
        $quantity_in_stock = 0;
        foreach($quantity as $n){
            $quantity_in_stock = $n->quantity;
        }
        if($quantity_in_stock < $req->numproduct){
            return redirect()->back()->with('message', 'Sorry! Our store is temporarily out of sizes suitable for this color.');
        }
        
        if(Auth::check()){
            $id = Auth::user()->id;
            $old_cart = DB::table('carts')->where([
                ['idUser', '=', $id],
                ['idProduct', '=', $req->idProduct],
                ['idSize', '=', $req->size],
                ['idColor', '=', $req->color],
            ])->select('quantity')->get();
            if(count($old_cart)==0){
                $cart = new Cart;
                $cart->idUser = $id;
                $cart->idProduct = $req->idProduct;
                $cart->idSize = $req->size;
                $cart->idColor = $req->color;
                $cart->quantity = $req->numproduct;
                $cart->save();
                return redirect('/shoppingcart');
            }
            else{
                $quantity_cart = 0; 
                foreach($old_cart as $n){
                    $quantity_cart = $n->quantity;
                }
                DB::table('carts')->where('idUser', $id)->update(array(
                    'quantity'=>$quantity_cart + $req->numproduct,
                ));
                return redirect('/shoppingcart');
            }
        }
        else{
            return redirect()->back()->with('message', 'You must login to add to cart');
        }
        
        
    }

    
    public function getSearch(Request $req){
        
        $productByCate = DB::table('products')->where('name','like','%'.$req->search.'%')->orWhere('unitPrice', $req->search)->paginate(8);
        return view('page.product', compact('productByCate'));
        // return view('page.product');
    }

    //Product 
    public function createPro(){
        $categories = Categorys::all();
        return view('page.admin.product.add-product')->with(compact('categories'));
    }

    public function storePro(){
        $data = request() -> validate([
            'idCategory'=>'required',
            'idImage'=>'required',
            'name'=>'required',
            'desc'=>'required',
            'gender'=>'required',
            'unitPrice'=>'required',
            'image'=>'required|image'
        ]);

        $imagePath = request('image') -> store('uploads', 'public');
        $product = new Products();
        $product->idCategory = $data['idCategory'];
        $product->idImage = $data['idImage'];
        $product->name = $data['name'];
        $product->desc = $data['desc'];
        $product->gender = $data['gender'];
        $product->unitPrice = $data['unitPrice'];
        $product->image = $imagePath;

        $product->save();
        return redirect('admin/dashboard');
    }

    public function listPro(){
        $products = DB::table('products')->join('categorys', 'products.idCategory', '=', 'categorys.id' )->select('products.*', 'categorys.name as categoryName')->get();

        $allProducts = view('page.admin.product.list-product')->with('products', $products);
        return view('page.admin_layout')->with('page.admin.product.list-product', $allProducts);
    }

    public function editPro($id){
        $categories = Categorys::all();
        $product = Products::find($id);
        $page_edit= view('page.admin.product.edit-product')->with(compact('categories', 'product'));
        
        return view('page.admin_layout')->with('page.admin.product.edit-product', $page_edit);
    }

    public function updatePro( $id)
    {
        $data = request() -> validate([
            'idCategory'=>'required',
            'idImage'=>'required',
            'name'=>'required',
            'desc'=>'required',
            'gender'=>'required',
            'unitPrice'=>'required',
        ]);

        $product = Products::find($id);
        $image = request('image');
        if($image){
            $imagePath = request('image') -> store('uploads', 'public');
            $product->idCategory = $data['idCategory'];
            $product->idImage = $data['idImage'];
            $product->name = $data['name'];
            $product->desc = $data['desc'];
            $product->gender = $data['gender'];
            $product->unitPrice = $data['unitPrice'];
            $product->image = $imagePath;
        }else{
            $product->idCategory = $data['idCategory'];
            $product->idImage = $data['idImage'];
            $product->name = $data['name'];
            $product->desc = $data['desc'];
            $product->gender = $data['gender'];
            $product->unitPrice = $data['unitPrice'];           
        }
        $product->save();
        return Redirect::to('admin/list-pro');
    }

    public function destroyPro($id)
    {
        DB::table('products')->where('id', $id)->delete();
        return Redirect::to('admin/list-pro');
    }

    //Product Detail
    public function createProDetail(){
        $products = Products::all();
        $sizes = Sizes::all();
        $colors = Colors::all();
        return view('page.admin.productDetail.add-productDetail')->with(compact('products', 'sizes', 'colors'));
    }

    public function storeProDetail(){
        $data = request() -> validate([
            'idProduct'=>'required',
            'idSize'=>'required',
            'idColor'=>'required',
            'quantity'=>'required'
        ]);

        $product = new ProductQuantity();
        $product->idProduct = $data['idProduct'];
        $product->idSize = $data['idSize'];
        $product->idColor = $data['idColor'];
        $product->quantity = $data['quantity'];

        $product->save();
        return redirect('admin/list-proDetail');
    }

    public function listProDetail(){
        $productDetails = DB::table('product_quantities')
            ->join('products', 'products.id', '=', 'product_quantities.idProduct' )
            ->join('sizes', 'sizes.id', '=', 'product_quantities.idSize')
            ->join('colors', 'colors.id', '=', 'product_quantities.idColor')
            ->select('products.name as productName','sizes.name as sizeName', 'colors.name as colorName', 'product_quantities.quantity', 'product_quantities.id')
            ->get();

        $allProductDetails = view('page.admin.productDetail.list-productDetail')->with('productDetails', $productDetails);
        return view('page.admin_layout')->with('page.admin.productDetail.list-productDetail', $allProductDetails);
    }

    public function editProDetail($id){
        $products = Products::all();
        $sizes = Sizes::all();
        $colors = Colors::all();
        $productDetail = ProductQuantity::find($id);
        $page_edit= view('page.admin.productDetail.edit-productDetail')->with(compact('products', 'sizes', 'colors', 'productDetail'));
        return view('page.admin_layout')->with('page.admin.productDetail.edit-productDetail', $page_edit);
    }

    public function updateProDetail($id)
    {
        $data = request() -> validate([
            'idProduct'=>'required',
            'idSize'=>'required',
            'idColor'=>'required',
            'quantity'=>'required'
        ]);

        $productDetail = ProductQuantity::find($id);
        $productDetail->idProduct = $data['idProduct'];
        $productDetail->idSize = $data['idSize'];
        $productDetail->idColor = $data['idColor'];
        $productDetail->quantity = $data['quantity'];

        $productDetail->save();
        return redirect('admin/list-proDetail');
    }

    public function destroyProDetail($id)
    {
        DB::table('product_quantities')->where('id', $id)->delete();
        return Redirect::to('admin/list-proDetail');
    }

    //Image Detail
    public function createImageDetail(){
        $imageDetails = ImageDetail::all();
        return view('page.admin.imageDetail.add-imageDetail')->with(compact('imageDetails'));
    }

    public function storeImageDetail(){
        $imagePath1 = request('image1') -> store('uploads', 'public');
        $imagePath2 = request('image2') -> store('uploads', 'public');
        $imagePath3 = request('image3') -> store('uploads', 'public');

        $product = new ImageDetail();
        $product->image1 = $imagePath1;
        $product->image2 = $imagePath2;
        $product->image3 = $imagePath3;

        $product->save();
        return redirect('admin/list-imageDetail');
    }
    
    public function listImageDetail(){
        $imageDetails = ImageDetail::all();
        return view('page.admin.imageDetail.list-imageDetail')->with(compact('imageDetails'));
    }

    public function editImageDetail($id){
        $productDetail = ImageDetail::find($id);
        $page_edit= view('page.admin.imageDetail.edit-imageDetail')->with(compact('productDetail'));
        return view('page.admin_layout')->with('page.admin.imageDetail.edit-imageDetail', $page_edit);
    }

    public function updateImageDetail($id)
    {
        $imageDetail = ImageDetail::find($id);
        $image1 = request('image1');
        $image2 = request('image2');
        $image3 = request('image3');
        if($image1){
            $imagePath1 = request('image1') -> store('uploads', 'public');
            $imageDetail->image1 = $imagePath1;
        }
        if($image2){
            $imagePath2 = request('image2') -> store('uploads', 'public');
            $imageDetail->image2 = $imagePath2;
        }
        if($image3){
            $imagePath3 = request('image3') -> store('uploads', 'public');
            $imageDetail->image3 = $imagePath3;
        }

        $imageDetail->save();
        return redirect('admin/list-imageDetail');
    }

    public function destroyImageDetail($id)
    {
        DB::table('image_details')->where('id', $id)->delete();
        return Redirect::to('admin/list-imageDetail');
    }
}
