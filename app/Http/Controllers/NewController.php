<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class newController extends Controller
{
    public function AuthLogin()
    {
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('admin/dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }

    public function index()
    {
        $this->AuthLogin();
        $all_news = DB::table('news')->get();
        $manage_news = view('page.admin.news.all_news')->with('all_news',$all_news);
        return view('page.admin_layout')->with('page.admin.news.all_news',$manage_news);
    }

    public function edit_new($id){
        $edit_new = DB::table('news')->where('id',$id)->first(); 
        $manage_new = view('page.admin.news.edit_new')->with('edit_new',$edit_new);
        return view('page.admin_layout')->with('page.admin.news.edit_new',$manage_new);
    }

    public function update_new(Request $requests, $id){
        $data = array();

        $data['title'] = $requests->title;
        $data['image'] = $requests->image;
        $data['description'] = $requests->description;

        DB::table('news')->where('id',$id)->update($data);
        Session::put('message','Update new success');
        return Redirect::to('news');
    }

    public function delete_new($id){
        DB::table('news')->where('id',$id)->delete();
        Session::put('message','Delete new success');
        return Redirect::to('news');
    }
}
