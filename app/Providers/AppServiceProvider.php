<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Categorys;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('page.home', function($view){
            $category = Categorys::all();
            $view->with('category', $category);
        });
        view()->composer('page.product', function($view){
            $category = Categorys::all();
            $view->with('category', $category);
        });
        view()->composer('header', function($view){
            if(!Auth::check()){
                // return redirect('/login');
            }
            else {
                $id = Auth::user()->id;
                $cart = DB::table('carts')
                ->join('Products', 'idProduct','=','Products.id')
                ->join('Sizes','idSize','=','Sizes.id')
                ->join('Colors','idColor','=','Colors.id')
                ->where('idUser','=',$id)
                ->select('carts.quantity', 
                    'Products.id as id',
                    'Products.name as productName', 
                    'Products.unitPrice as unitPrice', 
                    'Products.image as image', 
                    'Products.id as productid', 
                    'Sizes.name as sizeName', 
                    'Sizes.id as sizeid', 
                    'Colors.name as colorName',
                    'Colors.id as colorid')
                ->get();
                // print_r($cart);
                $view->with('cart', $cart);
            }
        });
        view()->composer('sub-header', function($view){
            if(!Auth::check()){
                // return redirect('/login');
            }
            else {
                $id = Auth::user()->id;
                $cart = DB::table('carts')
                ->join('Products', 'idProduct','=','Products.id')
                ->join('Sizes','idSize','=','Sizes.id')
                ->join('Colors','idColor','=','Colors.id')
                ->where('idUser','=',$id)
                ->select('carts.quantity', 
                    'Products.name as productName', 
                    'Products.id as id',
                    'Products.unitPrice as unitPrice', 
                    'Products.image as image', 
                    'Products.id as productid', 
                    'Sizes.name as sizeName', 
                    'Sizes.id as sizeid', 
                    'Colors.name as colorName',
                    'Colors.id as colorid')
                ->get();
                // print_r($cart);
                $view->with('cart', $cart);
            }
        });
    }
}
