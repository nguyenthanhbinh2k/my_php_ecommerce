<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageDetail extends Model
{
    protected $table = "Image_Details";
    protected $fillable = ['id', 'image1', 'image2', 'image3'];
}
