<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    
    protected $table = "Products";
    protected $fillable = ['idCategory', 'idImage', 'name', 'desc', 'gender', 'unitPrice', 'image'];

    public function category() {
        return $this->belongsTo('App\Models\Categorys', 'idCategory', 'id');
    }

    public function billDetail() {
        return $this->hasMany('App\Models\BillDetail', 'idProduct', 'id');
    }

    public function imageDetail() {
        return $this->belongsTo('App\Models\ImageDetail');
    }

    public function productQuantity() {
        return $this->hasMany('App\Models\ProductQuantity', 'idProduct', 'id');
    }
}
