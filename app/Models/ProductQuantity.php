<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductQuantity extends Model
{
    protected $table = "Product_Quantities";
    protected $fillable = ['id', 'idProduct', 'idSize', 'idColor', 'quantity'];

    public function products() {
        return $this->belongsTo('App\Models\Products', 'id', 'idProduct');
    }
}
