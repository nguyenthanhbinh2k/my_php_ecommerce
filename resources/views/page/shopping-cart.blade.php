@extends('master')
@section('content')
@section('title','Shopping Cart')
@include('sub-header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- breadcrumb -->
	<div class="container">
		<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
			<a href="home" class="stext-109 cl8 hov-cl1 trans-04">
				Home
				<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
			</a>

			<span class="stext-109 cl4">
				Shoping Cart
			</span>
		</div>
	</div>
		
			
	<!-- Shoping Cart -->
			@php
				$total = 0;
			@endphp
		<div class="container">
			@if (Session::has('message'))
				<div class="alert alert-danger">{{Session::get('message')}}</div>
			@endif
			<div class="row" style="justify-content: center">
				<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50" style="max-width: 100%">
					<div class="m-l-25 m-r--38 m-lr-0-xl">
						<form action="updatecart" method="POST">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<div class="wrap-table-shopping-cart" style="width: 1000px">
								<table class="table-shopping-cart">
									<tr class="table_head" style="display: flex; align-items: center; justify-content: space-around">
										<th>Product</th>
										<th >Name</th>
										<th >Color</th>
										<th>Size</th>
										<th>Price</th>
										<th>Quantity</th>
										<th>Total</th>
										<th>Delete</th>
									</tr>
									@if (isset($cart))
									@foreach ($cart as $item)
										@php
											$total += $item->unitPrice * $item->quantity;
										@endphp
									<tr class="table_row" style="display: flex; align-items: center; justify-content: space-around">
										<td >
											<div class="how-itemcart1">
												<img src="storage/{{$item->image}}" alt="IMG">
											</div>
										</td>
										<td><a href="/product-detail/{{$item->idProduct}}">{{$item->productName}} </a></td>
										<td>{{$item->colorName}} </td>
										<td>{{$item->sizeName}} </td>
										<td>{{number_format($item->unitPrice, 0, '', '.')}}</td>
										<td>
											{{-- {{$item->quantity}} --}}
											<div class="wrap-num-product flex-w m-l-auto m-r-0">
												<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
													<i class="fs-16 zmdi zmdi-minus"></i>
												</div>

												<input class="mtext-104 cl3 txt-center num-product" id="number-product" type="number" name="numproduct[{{$item->id}}]" value="{{$item->quantity}}">

												<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
													<i class="fs-16 zmdi zmdi-plus"></i>
												</div>
											</div>
										</td>
										<td class="column-7" id="total-detail">{{number_format($item->unitPrice * $item->quantity, 0, '', '.')}}</td>
										<td class="column-7">
											<a href="{{route('deletecart', $item->id)}}">
												<i class="fas fa-times fa-3x" style="color: red"></i>
											</a>
											
										</td>
									</tr>
									@endforeach
									@endif
								</table>
							</div>
							<div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm" style="justify-content: flex-end">
								<button type="submit" class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
									Update Cart
								</button>
							</div>
						</form>
					</div>
				</div>
				
				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<form class="bg0 p-t-75 p-b-85" action="addtobills" method="post">
						@if (count($errors)>0)
							@foreach ($errors->all() as $item)
							<div class="alert alert-danger">
								{{$item}}
							</div>
							@endforeach
						@endif
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
							<h4 class="mtext-109 cl2 p-b-30">
								Cart Totals
							</h4>

							<div class="flex-w flex-t bor12 p-b-13">
								<div class="size-208">
									<span class="stext-110 cl2">
										Subtotal:
									</span>
								</div>

								<div class="size-209">
									<span class="mtext-110 cl2">
										{{number_format($total, 0, '', '.')}}
									</span>
								</div>
							</div>

							<div class="flex-w flex-t bor12 p-t-15 p-b-30">
								<div class="size-208 w-full-ssm">
									<span class="stext-110 cl2">
										Shipping:
									</span>
								</div>

								<div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
									<p class="stext-111 cl6 p-t-2">
										There are no shipping methods available. Please double check your address, or contact us if you need any help.
									</p>
									
									<div class="p-t-15">
										<span class="stext-112 cl8">
											Calculate Shipping
										</span>
										<div class="bor8 bg0 m-b-12">
											<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="address" value="{{$user->address}}" placeholder="Address" required>
										</div>

										<div class="bor8 bg0 m-b-22">
											<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="number" name="phonnumber" value="{{$user->phonenumber}}"  placeholder="Phone Number" required>
										</div>
										
										{{-- <div class="flex-w">
											<div class="flex-c-m stext-101 cl2 size-115 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer">
												Update Totals
											</div>
										</div> --}}
											
									</div>
								</div>
							</div>

							<div class="flex-w flex-t p-t-27 p-b-33">
								<div class="size-208">
									<span class="mtext-101 cl2">
										Total:
									</span>
								</div>

								<div class="size-209 p-t-1">
									<span class="mtext-110 cl2">
										{{number_format($total, 0, '', '.')}}
									</span>
								</div>
							</div>

							<button class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 pointer" type="submit">
								Proceed to Checkout
							</button>
						</div>
					</form>
				</div>
			
			</div>
		</div>
	
		{{-- <script>
			function fnDown(){
				const sites = {!! json_encode($item->unitPrice) !!};
				const quantity = document.getElementById("number-product").value;
				if(quantity > 0){
					document.getElementById('total-detail').innerHTML = sites * (+quantity-1);

				}
			}
			function fnUp(){
				const sites = {!! json_encode($item->unitPrice) !!};
				const quantity = document.getElementById("number-product").value;
				if(quantity > 0){
					document.getElementById('total-detail').innerHTML = sites * (+quantity + 1);

				}
			}
		</script> --}}
@endsection