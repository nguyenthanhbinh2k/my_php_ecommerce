@extends('page.admin_layout')
@section('admin_content')
    <main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        DataTable Product Detail
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Product name</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Product name</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                    </tr>
                            </tfoot>
                            <tbody>
                                @foreach($productDetails as $key => $productDetail)
                                <tr>
                                    <td>{{$productDetail -> productName}}</td>
                                    <td>{{$productDetail -> sizeName}}</td>
                                    <td>{{$productDetail -> colorName}}</td>
                                    <td>{{$productDetail -> quantity}}</td>                                                              
                                    <td>
                                        <a href="{{URL::to('admin/edit-proDetail/'.$productDetail -> id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none; margin-right: 20px;">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a onclick="return confirm('Bạn có chắc là muốn xóa ko?')" href="{{URL::to('admin/delete-proDetail/'.$productDetail -> id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none;">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
@endsection
