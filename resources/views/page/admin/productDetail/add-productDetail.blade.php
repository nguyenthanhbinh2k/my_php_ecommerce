@extends('page.admin_layout')
@section('admin_content')
<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Add Product Detail
                    </div>
                    <div class="position-center">
                        <form role="form" action="{{URL::to('admin/store-proDetail')}}" method="post" enctype='multipart/form-data'>
                            {{ csrf_field() }}
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Product name</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="idProduct">
                                        <option value="0">Select Product</option>
                                        @foreach($products as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endforeach;
                                    </select>
                                    @error('idProduct')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Size</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="idSize">
                                        <option value="0">Select Size</option>
                                        @foreach($sizes as $size)
                                            <option value="{{$size->id}}">{{$size->name}}</option>
                                        @endforeach;
                                    </select>
                                    @error('idSize')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Color</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="idColor">
                                        <option value="0">Select Color</option>
                                        @foreach($colors as $color)
                                            <option value="{{$color->id}}">{{$color->name}}</option>
                                        @endforeach;
                                    </select>
                                    @error('idColor')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Quantity</label>
                                <div class="col-sm-10">
                                    <input type="text" name="quantity" class="form-control"  placeholder="Quantity">
                                    @error('quantity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>                           
                            <div class="align-self-center mx-auto">
                                <button type="submit" name="add_productDetail" class="btn btn-primary mb-2" style="margin-top:30px; ">Create</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
@endsection