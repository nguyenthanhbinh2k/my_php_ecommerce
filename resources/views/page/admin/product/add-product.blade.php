@extends('page.admin_layout')
@section('admin_content')

<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Add Product
                    </div>
                    <div class="position-center">
                        <form role="form" action="{{URL::to('admin/store-pro')}}" method="post" enctype='multipart/form-data'>
                            {{ csrf_field() }}
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Category name</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="idCategory">
                                        <option value="0">Select category</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach;
                                    </select>
                                    @error('idCategory')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Id image</label>
                                <div class="col-sm-10">
                                    <input type="text" name="idImage" class="form-control"  placeholder="Id image">
                                    @error('idImage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control"  placeholder="Name">
                                    @error('idImage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <input type="text" name="desc" class="form-control"  placeholder="Description">
                                    @error('idImage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Gender</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="gender">
                                        <option value="">Select gender</option>
                                        <option value="1">Male</option>
                                        <option value="0">Female</option>
                                    </select>
                                    @error('gender')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <input type="text" name="unitPrice" class="form-control"  placeholder="Price">
                                    @error('unitPrice')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image" class="form-control"  placeholder="Image">
                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="align-self-center mx-auto">
                                <button type="submit" name="add_product" class="btn btn-primary mb-2" style="margin-top:30px; ">Create</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
@endsection