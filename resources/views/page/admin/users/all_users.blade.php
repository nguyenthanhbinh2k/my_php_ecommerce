@extends('page.admin_layout')
@section('admin_content')
<!--  -->
<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        DataTable Users
                    </div>
                    <?php
                    $message = Session::get('message');
                    if($message){
                        echo '<span class="text-alert">'.$message.'</span>';
                        Session::put('message',null);
                    }
                    ?>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                  <th>Username</th>
                                  <th>Email</th>
                                  <th>Password</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Username</th>
                                  <th>Email</th>
                                  <th>Password</th>
                                  <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                              @foreach($all_users as $key => $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }} </td>
                                    <td>{{ $user->password }}</td>
                                    <td>
                                      <a href="{{URL::to('/edit-user/'.$user->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none; margin-right: 20px;">
                                        <i class="fas fa-edit"></i>
                                      </a>
                                      <a onclick="return confirm('Bạn có chắc là muốn xóa người dùng này ko?')" href="{{URL::to('/delete-user/'.$user->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none;">
                                        <i class="fas fa-trash-alt"></i>
                                      </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
<!--  -->
@endsection