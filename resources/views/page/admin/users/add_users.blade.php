@extends('page.admin_layout')
@section('admin_content')

<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Add User
                    </div>
                    <div class="position-center">
                        <form role="form" name="Form" action="{{URL::to('store-users')}}" onSubmit="return validateForm()" method="post" required>
                            {{ csrf_field() }}
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control"  placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="email" class="form-control"  placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Phone number</label>
                                <div class="col-sm-10">
                                    <input type="text" name="phonenumber" class="form-control"  placeholder="Phone number">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" name="address" class="form-control"  placeholder="Address">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" class="form-control"  placeholder="Password">
                                </div>
                            </div>
                            <div class="align-self-center mx-auto">
                                <button type="submit" name="add_users" class="btn btn-primary mb-2" style="margin-top:30px; ">Add user</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
    <script type="text/javascript">
        function validateForm() {
            var name = document.forms["Form"]["name"].value;
            var email = document.forms["Form"]["email"].value;
            var password = document.forms["Form"]["password"].value;
            if (name == "" || email == "" || password == "" || name == null || email == null || password == null ) {
                alert("Please Fill All Required Field");
                return false;
            }
            if (!validateEmail(email)) {
                alert("Email is not valid");
                return false;
            }
            if (!validatePassword(password)) {
                alert("Password is not valid");
                return false;
            }
        }
        function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        function validatePassword(pw) {
            const pattern = /^(?=.{5,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W])/
            return pattern.test(pw);
            }
    </script>
@endsection