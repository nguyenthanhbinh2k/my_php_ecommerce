@extends('page.admin_layout')
@section('admin_content')
<!--  -->
<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        DataTable Colors
                    </div>
                    @if (Session::has('message'))
                      <div class="alert alert-danger">{{Session::get('message')}}</div>
                      {{Session::put('message',null)}}
                    @endif
                    <div class="card-body">
                        <table id="datatablesSimple">
                        <thead>
                                <tr>
                                  <th>Id</th>
                                  <th>Title</th>
                                  <th>Image</th>
                                  <th>Description</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Id</th>
                                  <th>Title</th>
                                  <th>Image</th>
                                  <th>Description</th>
                                </tr>
                            </tfoot>
                            <tbody>
                              @foreach($all_news as $key => $new)
                                <tr>
                                    <td>{{ $new->id }}</td>
                                    <td>{{ $new->title }} </td>
                                    <td>{{ $new->image }} </td>
                                    <td>{{ $new->description }} </td>
                                    <td>
                                      <a href="{{URL::to('/edit-new/'.$new->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none; margin-right: 20px;">
                                        <i class="fas fa-edit"></i>
                                      </a>
                                      <a onclick="return confirm('Bạn có chắc là muốn xóa người dùng này ko?')" href="{{URL::to('/delete-new/'.$new->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none;">
                                        <i class="fas fa-trash-alt"></i>
                                      </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
<!--  -->
@endsection