@extends('page.admin_layout')
@section('admin_content')
<!--  -->
<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <br>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        DataTable Customer Infomation
                    </div>
                    <?php
                    $message = Session::get('message');
                    if($message){
                        echo '<span class="text-alert">'.$message.'</span>';
                        Session::put('message',null);
                    }
                    ?>
                    <div class="card-body">
                        <div>
                            <span>Customer: {{ $order_by_id->first()->userName }}</span>
                            <br>
                            <span>Place of receipt: {{ $order_by_id->first()->placeOfReceipt }}</span>
                            <br>
                            <span>Phone number: {{ $order_by_id->first()->phoneOfReceipt }}</span>
                        </div>
                        <br>
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                  <th>Product name</th>
                                  <th>Color</th>
                                  <th>Size</th>
                                  <th>Quantity</th>
                                  <th>Total</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Product name</th>
                                  <th>Color</th>
                                  <th>Size</th>
                                  <th>Quantity</th>
                                  <th>Total</th> 
                                </tr>
                            </tfoot>
                            <tbody>
                              @foreach($order_by_id as $key => $order)
                                <tr>
                                    <td>{{ $order->name }} </td>
                                    <td>{{ $order->colorName }} </td>
                                    <td>{{ $order->sizeName }} </td>
                                    <td>{{ $order->productQuantity }} </td>
                                    <td>{{ $order->productQuantity * $order->productUnitPrice }}</td>
                                    <td>
                                      <a href="{{URL::to('/view-order'.$order->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none; margin-right: 20px;">
                                        <i class="fas fa-edit"></i>
                                      </a>
                                      <a onclick="return confirm('Bạn có chắc là muốn xóa người dùng này ko?')" href="{{URL::to('/delete-order/'.$order->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none;">
                                        <i class="fas fa-trash-alt"></i>
                                      </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>          
            <!-- end page_loader -->
        </div>
        
    </main>
<!--  -->
@endsection