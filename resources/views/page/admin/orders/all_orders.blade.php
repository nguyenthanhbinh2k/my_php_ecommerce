@extends('page.admin_layout')
@section('admin_content')
<!--  -->
<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        DataTable Orders
                    </div>
                    @if (Session::has('message'))
                      <div class="alert alert-danger">{{Session::get('message')}}</div>
                      {{Session::put('message',null)}}
                    @endif
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                  <th>Id</th>
                                  <th>Customer name</th>
                                  <th>Status</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Id</th>
                                  <th>Customer name</th>
                                  <th>Status</th>
                                </tr>
                            </tfoot>
                            <tbody>
                              @foreach($all_orders as $key => $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->name }} </td>
                                    <td>
                                      {{ $order->status }} 
                                      @if($order->status == 'pending')
                                        <a href="{{URL::to('/approve-status/'.$order->id)}}">
                                          <i class="fas fa-check" style="color: green;"></i> 
                                        </a>
                                        <a href="{{URL::to('/decline-status/'.$order->id)}}">
                                          <i class="fas fa-times" style="color: red;"></i> </td>
                                        </a>
                                      @endif
                                    <td>
                                      <a href="{{URL::to('/view-order/'.$order->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none; margin-right: 20px;">
                                        <i class="fas fa-edit"></i>
                                      </a>
                                      <a onclick="return confirm('Bạn có chắc là muốn xóa người dùng này ko?')" href="{{URL::to('/delete-order/'.$order->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none;">
                                        <i class="fas fa-trash-alt"></i>
                                      </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
<!--  -->
@endsection