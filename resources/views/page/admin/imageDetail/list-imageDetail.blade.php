@extends('page.admin_layout')
@section('admin_content')
    <main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        DataTable Image Detail
                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Image 1</th>
                                    <th>Image 2</th>
                                    <th>Image 3</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>Image 1</th>
                                <th>Image 2</th>
                                <th>Image 3</th>
                                <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($imageDetails as $imageDetail)
                                <tr>
                                    <td>{{$imageDetail->id}}</td>
                                    <td><img src="{{URL::to('storage/'.$imageDetail->image1)}}" style="width:100px; height:130px;"/></td>
                                    <td><img src="{{URL::to('storage/'.$imageDetail->image2)}}" style="width:100px; height:130px;"/></td>
                                    <td><img src="{{URL::to('storage/'.$imageDetail->image3)}}" style="width:100px; height:130px;"/></td>
                                    <td>
                                        <a href="{{URL::to('admin/edit-imageDetail/'.$imageDetail->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none; margin-right: 20px;">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a onclick="return confirm('Bạn có chắc là muốn xóa ko?')" href="{{URL::to('admin/delete-imageDetail/'.$imageDetail->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none;">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
@endsection
