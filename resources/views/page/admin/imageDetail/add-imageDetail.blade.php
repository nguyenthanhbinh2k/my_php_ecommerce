@extends('page.admin_layout')
@section('admin_content')

<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Add Image Detail
                    </div>
                    <div class="position-center">
                        <form role="form" action="{{URL::to('admin/store-imageDetail')}}" method="post" enctype='multipart/form-data'>
                            {{ csrf_field() }}
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Image1</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image1" class="form-control"  placeholder="Image">
                                    @error('image1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Image 2</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image2" class="form-control"  placeholder="Image">
                                    @error('image2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Image 3</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image3" class="form-control"  placeholder="Image">
                                    @error('image3')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="align-self-center mx-auto">
                                <button type="submit" name="add_product" class="btn btn-primary mb-2" style="margin-top:30px; ">Create</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
@endsection