@extends('page.admin_layout')
@section('admin_content')
    <main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Edit Slide  
                    </div>
                    <div class="position-center">
                        <form role="form" name="Form" action="{{URL::to('/admin/update-slide/'.$edit_slide->id)}}" onSubmit="return validateForm()" method="post" required>
                            {{ csrf_field() }}
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" name="title" value="{{$edit_slide->title}}"  class="form-control"  placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="text" value="{{$edit_slide->image}}" name="image" class="form-control"  placeholder="Email" disabled>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Thumb</label>
                                <div class="col-sm-10">
                                    <input type="text" name="thumb" value="{{$edit_slide->thumb}}"  class="form-control"  placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="align-self-center mx-auto">
                                <button type="submit" class="btn btn-primary mb-2" style="margin-top:30px; ">Save Change</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
    <script type="text/javascript">
        function validateForm() {
            var a = document.forms["Form"]["title"].value;
            var b = document.forms["Form"]["image"].value;
            var c = document.forms["Form"]["thumb"].value;
            if (a == "" || b == "" || c == "") {
                alert("Please Fill All Required Field");
                return false;
            }
        }
    </script>
@endsection