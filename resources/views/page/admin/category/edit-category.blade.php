@extends('page.admin_layout')
@section('admin_content')

<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Edit Category
                    </div>
                    <div class="position-center">
                        <form role="form" action="{{URL::to('admin/update-category/'.$category->id)}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group row" style="margin-top:30px;">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Category name</label>
                                <div class="col-sm-10">
                                    <input type="text" value="{{$category->name}}" name="name" class="form-control"  placeholder="Category">
                                </div>
                            </div>
                            <div class="align-self-center mx-auto">
                                <button type="submit" name="edit_category" class="btn btn-primary mb-2" style="margin-top:30px; ">Save change</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
@endsection