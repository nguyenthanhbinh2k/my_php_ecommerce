@extends('page.admin_layout')
@section('admin_content')
<!--  -->
<main>
        <div class="container-fluid px-4" >
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <!-- page_loader -->
            <div id="page_loader">                    
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        DataTable Colors
                    </div>
                    @if (Session::has('message'))
                      <div class="alert alert-danger">{{Session::get('message')}}</div>
                      {{Session::put('message',null)}}
                    @endif
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                  <th>Id</th>
                                  <th>Color name</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Id</th>
                                  <th>Color name</th>
                                </tr>
                            </tfoot>
                            <tbody>
                              @foreach($all_colors as $key => $color)
                                <tr>
                                    <td>{{ $color->id }}</td>
                                    <td>{{ $color->name }} </td>
                                    <td>
                                      <a href="{{URL::to('/admin/edit-color/'.$color->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none; margin-right: 20px;">
                                        <i class="fas fa-edit"></i>
                                      </a>
                                      <a onclick="return confirm('Are you sure about deleting this color?')" href="{{URL::to('/admin/delete-color/'.$color->id)}}" class="active styling-edit" ui-toggle-class="" style="text-decoration: none;">
                                        <i class="fas fa-trash-alt"></i>
                                      </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end page_loader -->
        </div>
    </main>
<!--  -->
@endsection