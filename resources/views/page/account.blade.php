@extends('master')
@section('content')
@section('title','Profile')
@include('sub-header')

<head>
    <link rel="stylesheet" title="style" href="source/assets/dest/css/style.css">
</head>

<div class="container">
    <div id="content">
        <form  action="/updateprofile" method="post" >
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <h4>Profile</h4>
                    <div class="space20">&nbsp;</div>
                    <div class="form-block">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{$user->name}}" required>
                    </div>
                    
                    <div class="form-block">
                        <label for="phonenumber">Phone Number</label>
                        <input type="number" name="phonenumber" value="{{$user->phonenumber}}" style="border: 1px solid #e1e1e1; height: 37px; padding: 0 12px" required>
                    </div>
                    <div class="form-block">
                        <label for="address">Address</label>
                        <input type="text" name="address" value="{{$user->address}}" style="border: 1px solid #e1e1e1; height: 37px; padding: 0 12px" required>
                    </div>
                    <div class="form-block" style="display: flex; justify-content: center">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
                
            </div>
        </form>
    </div> <!-- #content -->
</div> <!-- .container -->
@endsection