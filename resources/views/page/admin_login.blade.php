<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="{{asset('')}}" >
    <link href="source/admin/css/login.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/4a22766421.js" crossorigin="anonymous"></script>
    <title>Login</title>
</head>
<body>
    <div class='login'>
        <form class='form-login' action="{{URL::to('/admin-dashboard')}}" method="post">
            {{ csrf_field() }}
            <h2>Login</h2>
            <input 
                type='text' 
                placeholder='Email'  
                name="admin_email" 
                required
            />
            <input 
                type='password' 
                placeholder='Password' 
                name='admin_password'
                required
            />
            <a href="register.php" >Sign up?</a>
            <?php
                $message = Session::get('message');
                if($message){
                    echo '<span class="text-alert">'.$message.'</span>';
                    Session::put('message', null);
                }
            ?>
            <button type="submit" class="submit-login" name="login">Login</button>
            <div class="social-network">
                <ul>
                    <li><a href=""><i class="fab fa-facebook"></i></a></li>
                    <li><a href=""><i class="fab fa-twitter"></i></a></li>
                    <li><a href=""><i class="fab fa-instagram"></i></a></li>
                    <li><a href=""><i class="fab fa-google-plus"></i></a></li>
                </ul>
            </div>
        </form>

    </div>
</body>
</html>
