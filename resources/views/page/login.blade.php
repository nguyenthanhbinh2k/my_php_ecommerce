@extends('master')
@section('content')
@section('title','Login')
@include('sub-header')

<head>
    <link rel="stylesheet" title="style" href="source/assets/dest/css/style.css">
</head>

<div class="container">
    <div id="content">
        <form action="/login" method="post" class="beta-form-checkout">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            @if (count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $item)
                            {{$item}}
                        @endforeach
                    </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-danger">{{Session::get('message')}}</div>
            @endif
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <h4>Login</h4>
                    <div class="space20">&nbsp;</div>

                    
                    <div class="form-block">
                        <label for="email">Email address*</label>
                        <input type="email" name="email" required>
                    </div>
                    <div class="form-block">
                        <label for="password">Password*</label>
                        <input type="password" name="password" style="border: 1px solid #e1e1e1; height: 37px; padding: 0 12px" required>
                    </div>
                    <div class="form-block">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                    <div class="col-sm-3"><a href="{{route('register')}}">Register?</a></div>
                </div>
                
            </div>
        </form>
    </div> <!-- #content -->
</div> <!-- .container -->
@endsection