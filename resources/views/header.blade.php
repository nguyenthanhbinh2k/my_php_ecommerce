<header class="header-v3">
    <!-- Header desktop -->
    <div class="container-menu-desktop trans-03">
        <div class="wrap-menu-desktop">
            <nav class="limiter-menu-desktop p-l-45">
                
                <!-- Logo desktop -->		
                <a href="home" class="logo">
                    <img src="source/images/icons/logo-02.png" alt="IMG-LOGO">
                </a>

                <!-- Menu desktop -->
                <div class="menu-desktop">
                    <ul class="main-menu">
                        <li>
                            <a href="home">Home</a>
                        </li>

                        <li>
                            <a href="product/0">Shop</a>
                        </li>

                        <li>
                            <a href="shoppingcart">Cart</a>
                        </li>

                        <li>
                            <a href="blog">Blog</a>
                        </li>

                        <li>
                            <a href="about">About</a>
                        </li>

                        <li>
                            <a href="contact">Contact</a>
                        </li>
                    </ul>
                </div>	

                <!-- Icon header -->
                <div class="wrap-icon-header flex-w flex-r-m h-full">							
                    <div class="flex-c-m h-full p-r-25 bor6">
                        @if (isset($cart))
                        <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="{{count($cart)}}">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                        @else
                        <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="0">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                        @endif
                        
                    </div>
                        
                    <div class="flex-c-m h-full p-lr-19">
                        <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 js-show-sidebar">
                            <i class="zmdi zmdi-menu"></i>
                        </div>
                    </div>
                </div>
            </nav>
        </div>	
    </div>

    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->		
        <div class="logo-mobile">
            <a href="index.html"><img src="source/images/icons/logo-01.png" alt="IMG-LOGO"></a>
        </div>

        <!-- Icon header -->
        <div class="wrap-icon-header flex-w flex-r-m h-full m-r-15">
            <div class="flex-c-m h-full p-r-5">
                @if (isset($cart))
                        <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="{{count($cart)}}">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                        @else
                        <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" data-notify="0">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                        @endif
            </div>
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </div>
    </div>


    <!-- Menu Mobile -->
    <div class="menu-mobile">
        <ul class="main-menu-m">
            <li>
                <a href="home">Home</a>
            </li>

            <li>
                <a href="product/0">Shop</a>
            </li>

            <li class="label1" data-label1="hot">
                <a href="shopping-cart">Features</a>
            </li>

            <li>
                <a href="blog">Blog</a>
            </li>

            <li>
                <a href="about">About</a>
            </li>

            <li>
                <a href="contact">Contact</a>
            </li>
        </ul>
    </div>

    <!-- Modal Search -->
    <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
        <button class="flex-c-m btn-hide-modal-search trans-04">
            <i class="zmdi zmdi-close"></i>
        </button>

        <form class="container-search-header">
            <div class="wrap-search-header">
                <input class="plh0" type="text" name="search" placeholder="Search...">

                <button class="flex-c-m trans-04">
                    <i class="zmdi zmdi-search"></i>
                </button>
            </div>
        </form>
    </div>
    <!-- Cart -->
	<div class="wrap-header-cart js-panel-cart">
		<div class="s-full js-hide-cart"></div>

		<div class="header-cart flex-col-l p-l-65 p-r-25">
			<div class="header-cart-title flex-w flex-sb-m p-b-8">
				<span class="mtext-103 cl2">
					Your Cart
				</span>

				<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
					<i class="zmdi zmdi-close"></i>
				</div>
			</div>
			
			<div class="header-cart-content flex-w js-pscroll">
				<ul class="header-cart-wrapitem w-full">
                    @if (isset($cart))
                        @foreach ($cart as $item)
                            <li class="header-cart-item flex-w flex-t m-b-12">
                                <div class="header-cart-item-img">
                                    <img src="storage/{{$item->image}}" alt="IMG">
                                </div>

                                <div class="header-cart-item-txt p-t-8">
                                    <a href="/product-detail/{{$item->id}}" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
                                        {{$item->productName}}
                                    </a>

                                    <span class="header-cart-item-info">
                                        {{$item->quantity}} x {{number_format($item->unitPrice, 0, '', '.')}}
                                    </span>
                                </div>
                            </li>
                        @endforeach
                    @endif
				</ul>
				
				<div class="w-full">
					<div class="header-cart-total w-full p-tb-40">
						{{-- Total:  @if (Session::has('cart')) {{Session('cart')->totalPrice}} @endif --}}
                        {{-- : Quantity: {{Session('cart')->totalQty}} --}}
					</div>

					<div class="header-cart-buttons flex-w w-full"  style="justify-content: center">
						<a href="shoppingcart" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
							View Cart
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>


</header>

