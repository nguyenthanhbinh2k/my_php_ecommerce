<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;


//page users

Route::get('/', function () {
    return redirect('home');
});

Route::get('/home', 
    [
        'as'=>'home',
        'uses'=>'ProductController@getHome',
    ]
);

Route::get('product/{type}', 
    [
        'as'=>'product',
        'uses'=>'ProductController@getProduct'
    ]
);

Route::get('product-detail/{id}', 
    [
        'as'=>'product-detail',
        'uses'=>'ProductController@getProductDetail'
    ]
);

Route::get('contact', 
    [
        'as'=>'contact',
        'uses'=>'ContactController@contact'
    ]
);

Route::get('blog', 
    [
        'as'=>'blog',
        'uses'=>'BlogController@blog'
    ]
);

Route::get('about', 
    [
        'as'=>'about',
        'uses'=>'AboutController@about'
    ]
);

Route::get('shoppingcart', 
    [
        'as'=>'shoppingcart',
        'uses'=>'CartController@getCartuser'
    ]
);

Route::get('deletecart/{id}',
    [
        'as'=>'deletecart',
        'uses'=>'CartController@deleteProduct'
    ]
);



Route::get('login', 
    [
        'as'=>'login',
        'uses'=>'LoginController@getLogin'
    ]
);

Route::post('login', 
    [
        'as'=>'login',
        'uses'=>'LoginController@postLogin'
    ]
);

Route::get('logoutuser',
    [
        'as'=>'logoutuser',
        'uses' => 'LoginController@logoutuser'
    ]
);

Route::get('register', 
    [
        'as'=>'register',
        'uses'=>'RegisterController@getRegiser'
    ]
);

Route::post('register', 
    [
        'as'=>'register',
        'uses'=>'RegisterController@postRegister'
    ]
);

Route::get('search', 
    [
        'as'=>'search',
        'uses'=>'ProductController@getSearch'
    ]
);


Route::get('add-to-cart',
    [
        'as'=>'addToCart',
        'uses'=>'ProductController@getAddToCart'
    ]
);


Route::post('addtocart', 
    [
        'as'=>'addtocart',
        'uses' => 'ProductController@addToCart'
    ]
);


Route::post('addtobills',
    [
        'as'=>'addtobills',
        'uses'=>'AddBillsController@addBill'
    ]
);


Route::post('updatecart',
    [
        'as'=>'updatecart',
        'uses'=>'CartController@updateCart'
    ]
);

Route::get('account', 
    [
        'as' => 'account',
        'uses' => 'AccountController@getAccount'
    ]
);

Route::post('updateprofile',
    [
        'as'=>'updateprofile',
        'uses'=>'AccountController@updateProfile'
    ]
);

//Admin

Route::get('admin/dashboard',
    [
        'as'=>'AdminDashboard',
        'uses'=>'AdminController@dashboard'
    ]
);

Route::get('admin',
    [
        'as'=>'AdminLogin',
        'uses'=>'AdminController@login'
    ]
);

Route::post('admin-dashboard',
    [
        'as'=>'AdminLogin',
        'uses'=>'AdminController@show_dashboard'
    ]
);

Route::get('/logout', 'AdminController@logout');

//User 
Route::get('users','UserController@index');
Route::get('add-users','UserController@add_users');
Route::post('store-users','UserController@store_users');
Route::get('/edit-user/{id}','UserController@edit_user');

Route::post('/update-user/{id}','UserController@update_user');
Route::get('/delete-user/{id}','UserController@delete_user');

//Color
Route::get('admin/colors','ColorController@index');
Route::get('admin/delete-color/{id}','ColorController@delete_color');
Route::get('admin/edit-color/{id}','ColorController@edit_color');
Route::post('admin/update-color/{id}','ColorController@update_color');

//Size
Route::get('admin/sizes','SizeController@index');
Route::get('admin/delete-size/{id}','SizeController@delete_size');
Route::get('admin/edit-size/{id}','SizeController@edit_size');
Route::post('admin/update-size/{id}','SizeController@update_size');

//Slide
Route::get('admin/slides','SlideController@index');
Route::get('admin/delete-slide/{id}','SlideController@delete_slide');
Route::get('admin/edit-slide/{id}','SlideController@edit_slide');
Route::post('admin/update-slide/{id}','SlideController@update_slide');


//New
Route::get('news','NewController@index');
Route::get('/delete-new/{id}','NewController@delete_new');
Route::get('/edit-new/{id}','NewController@edit_new');

//Order
Route::get('manage-order','OrderController@index');
Route::get('/view-order/{id}','OrderController@view_order');
Route::get('approve-status/{id}','OrderController@approve_status');
Route::get('decline-status/{id}','OrderController@decline_status');
// commit
//admin_pages

Route::get('admin/list-category', 
    [
        'as'=>'listCategory',
        'uses'=>'CategoryController@index',
    ]
);

Route::get('admin/add-category','CategoryController@create');
Route::post('admin/store-category','CategoryController@store');
Route::get('admin/edit-category/{id}','CategoryController@edit');
Route::post('admin/update-category/{id}','CategoryController@update');
Route::get('admin/delete-category/{id}','CategoryController@destroy');

// product
Route::get('admin/list-pro', 'ProductController@listPro');
Route::get('admin/add-pro', 'ProductController@createPro');
Route::post('admin/store-pro','ProductController@storePro');
Route::get('admin/edit-pro/{id}','ProductController@editPro');
Route::post('admin/update-pro/{id}','ProductController@updatePro');
Route::get('admin/delete-pro/{id}','ProductController@destroyPro');

//product quantity
Route::get('admin/list-proDetail', 'ProductController@listProDetail');
Route::get('admin/add-proDetail', 'ProductController@createProDetail');
Route::post('admin/store-proDetail','ProductController@storeProDetail');
Route::get('admin/edit-proDetail/{id}','ProductController@editProDetail');
Route::post('admin/update-proDetail/{id}','ProductController@updateProDetail');
Route::get('admin/delete-proDetail/{id}','ProductController@destroyProDetail');
// Mail
Route::get('send-email', 'EmailController@sendEMail');

//image detail
Route::get('admin/list-imageDetail', 'ProductController@listImageDetail');
Route::get('admin/add-imageDetail', 'ProductController@createImageDetail');
Route::post('admin/store-imageDetail','ProductController@storeImageDetail');
Route::get('admin/edit-imageDetail/{id}','ProductController@editImageDetail');
Route::post('admin/update-imageDetail/{id}','ProductController@updateImageDetail');
Route::get('admin/delete-imageDetail/{id}','ProductController@destroyImageDetail');
